## [1.1.1](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.1.0...v1.1.1) (2020-11-29)


### Bug Fixes

* update request status type ([91528a4](https://gitlab.com/urbanlink/openpublicatie-api/commit/91528a4f0d573ea5cfbe944340d763b3e59a9da6))
* **organization:** add new organization fields ([aa857e9](https://gitlab.com/urbanlink/openpublicatie-api/commit/aa857e92a5984322d87ef77e3ce87ffd8163a910))

# [1.1.0](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.8...v1.1.0) (2020-11-29)


### Bug Fixes

* add db mirgration ([8f6f65e](https://gitlab.com/urbanlink/openpublicatie-api/commit/8f6f65e23c428c1621d74f318c8af8f0ddcefb9b))
* add organization to request ([bfdcf9f](https://gitlab.com/urbanlink/openpublicatie-api/commit/bfdcf9f60777190f5bc4fa5add8c9270a0fb7e9d))
* update packages ([cfdaa4e](https://gitlab.com/urbanlink/openpublicatie-api/commit/cfdaa4ebbde693a44d292bef408a48b919c20397))
* update request relation ([87dce12](https://gitlab.com/urbanlink/openpublicatie-api/commit/87dce12f714152ed58ef5ec4058ac5530fa97cbf))


### Features

* add comment module ([28c4768](https://gitlab.com/urbanlink/openpublicatie-api/commit/28c4768d1d3dd025034140d1efd1779628e03ff2))
* add communication module ([1736b8e](https://gitlab.com/urbanlink/openpublicatie-api/commit/1736b8e1baf7d0e507d32d9f41970e3d4e411c5e))
* add notification module ([351f7fa](https://gitlab.com/urbanlink/openpublicatie-api/commit/351f7fa5d28f0928e478b49e4967a8ea6a03bfb9))
* add publication module ([0f4f575](https://gitlab.com/urbanlink/openpublicatie-api/commit/0f4f575767c90e56749ab10f4f8880ffed73ad60))

## [1.0.8](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.7...v1.0.8) (2020-11-26)


### Bug Fixes

* make slug optional on request create ([43933fd](https://gitlab.com/urbanlink/openpublicatie-api/commit/43933fd97dc552e769e6a1517ec357b8c523eb23))

## [1.0.7](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.6...v1.0.7) (2020-11-25)


### Bug Fixes

* remove sentry package ([dd9200b](https://gitlab.com/urbanlink/openpublicatie-api/commit/dd9200b5b6cfefcd4986e412f3f6e406ca51069c))

## [1.0.6](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.5...v1.0.6) (2020-11-25)


### Bug Fixes

* add slug automatically ([60191fd](https://gitlab.com/urbanlink/openpublicatie-api/commit/60191fd130d0d76b2ee7ab3133472814f4a8addb))
* update sentry settings ([54e5bcc](https://gitlab.com/urbanlink/openpublicatie-api/commit/54e5bccc90d82a13a71f6e2faca6ff9ce36f726a))

## [1.0.5](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.4...v1.0.5) (2020-11-25)


### Bug Fixes

*  add sentry interceptor ([f8db913](https://gitlab.com/urbanlink/openpublicatie-api/commit/f8db91379c628d623e32a773a3fca14cfc443a90))

## [1.0.4](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.3...v1.0.4) (2020-11-23)


### Bug Fixes

* remove unneeded env vars ([3387bb4](https://gitlab.com/urbanlink/openpublicatie-api/commit/3387bb4dceefd6e5cbc159ecb8bb2807d3bd0cb0))

## [1.0.3](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.2...v1.0.3) (2020-11-23)


### Bug Fixes

* update env ([a0ef9eb](https://gitlab.com/urbanlink/openpublicatie-api/commit/a0ef9eb531ea81fc222e619ff90af50fbb3f84f9))

## [1.0.2](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.1...v1.0.2) (2020-11-23)


### Bug Fixes

* update request service ([a3d3722](https://gitlab.com/urbanlink/openpublicatie-api/commit/a3d3722df8148fa45374929b557aac43faad014e))
* **db:** migrations ([5b764ba](https://gitlab.com/urbanlink/openpublicatie-api/commit/5b764ba8cf32a08802306ef480fcef0e161e96d9))
* update auth and user ([8003b87](https://gitlab.com/urbanlink/openpublicatie-api/commit/8003b875fdd13ed89c077edb10301e0c399ca021))

## [1.0.1](https://gitlab.com/urbanlink/openpublicatie-api/compare/v1.0.0...v1.0.1) (2020-11-15)


### Bug Fixes

* **ci:** disable deploy script in SR job ([a1ff2d6](https://gitlab.com/urbanlink/openpublicatie-api/commit/a1ff2d6fe39214ba0819950dc5ff7f64e3cc8e6b))

# 1.0.0 (2020-11-15)


### Bug Fixes

* don;t type check modules ([8da4c25](https://gitlab.com/urbanlink/openpublicatie-api/commit/8da4c253e1659926f9292ac0f9e2f7c98bbf5113))
* **ci:** make release job blokcing the pipeline ([ddb2eff](https://gitlab.com/urbanlink/openpublicatie-api/commit/ddb2eff80864577cc00ac686bb0636e667af6e2c))
* update gitlab-ci ([c0171e2](https://gitlab.com/urbanlink/openpublicatie-api/commit/c0171e22f62eabd1f150305d58e342f328f0ce6c))
* update typescript ([24ef975](https://gitlab.com/urbanlink/openpublicatie-api/commit/24ef975d5b7ba444e1a49ae4b2525b80ce83d5bd))


### Features

* **core:** convert to nestjs modules ([fd5d906](https://gitlab.com/urbanlink/openpublicatie-api/commit/fd5d9062fcf9b71ed2120f943a00ace0fbd3e6d3))
* **core:** migrate to nestjs setup ([14a629a](https://gitlab.com/urbanlink/openpublicatie-api/commit/14a629ac03b4c05f07bf5065444bd894cd3bafb1))
