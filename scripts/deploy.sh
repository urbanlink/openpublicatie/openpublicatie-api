#!/bin/bash

source /opt/openpublicatie/backend/config/.env

docker stop $DOCKER_CONTAINER_NAME
docker rm -f $DOCKER_CONTAINER_NAME

echo "docker login"
docker login $DOCKER_REGISTRY -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_TOKEN

echo "docker pull"
docker pull $DOCKER_REGISTRY_CONTAINER_URL

echo "docker run"
docker run -d -p $PORT:$PORT --env-file $DOCKER_ENV_FILE --net $DOCKER_NETWORK --name $DOCKER_CONTAINER_NAME $DOCKER_REGISTRY_CONTAINER_URL

echo "Deplpoy done"