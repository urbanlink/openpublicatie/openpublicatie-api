import { Module } from '@nestjs/common';
import Joi from 'joi';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { AppController } from './app.controller';
import { DatabaseModules } from './modules/database/database.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { MailModule } from './modules/mail/mail.module';
import { RequestModule } from './modules/request/request.module';
import { OrganizationModule } from './modules/organization/organization.module';
import { CommunicationModule } from './modules/communication/communication.module';
import { CommentModule } from './modules/comment/comment.module';
import { NotificationModule } from './modules/notification/notification.module';
import { PublicationModule } from './modules/publication/publication.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USERNAME: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE: Joi.string().required(),
        DB_SYNC: Joi.boolean().required(),
        DB_LOG: Joi.boolean().required(),
        PORT: Joi.number()
      }),
    }),
    DatabaseModules,
    MailModule.register(),
    UserModule,
    AuthModule,
    RequestModule,
    OrganizationModule,
    CommunicationModule,
    CommentModule,
    NotificationModule,
    PublicationModule
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
  constructor(private readonly configService: ConfigService) {
    // const a = {
    //     type: 'postgres',
    //     host: configService.get('DB_HOST'),
    //     port: configService.get('DB_PORT'),
    //     username: configService.get('DB_USERNAME'),
    //     password: configService.get('DB_PASSWORD'),
    //     database: configService.get('DB_DATABASE'),
    //     entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    //     migrations: [__dirname + '/migrations/*{.ts,.js}'],
    //     synchronize: configService.get('DB_SYNC'),

    //     logging: configService.get('DB_LOG'),

    //     cli: {
    //       entitiesDir: 'src',
    //       migrationsDir: 'src/database/migrations',
    //       subscribersDir: 'src/database/subscriber',
    //     },
    // }
    // console.log(a);
  }
}
