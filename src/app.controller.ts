import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  
  @Get()
  root() {
    return {
      title: 'Open Publicatie API',
      version: process.env.npm_package_version,
      environment: process.env.NODE_ENV,
    };
  }
}
