import { NestFactory } from '@nestjs/core';
import * as Sentry from '@sentry/node';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import helmet from 'helmet';
import { ValidationError } from 'class-validator';
import { AppModule } from './app.module';
import { SentryInterceptor } from './sentry.interceptor';

// import * as p from '../package.json'; 

async function bootstrap() {

  const appOptions = { cors: true }; 
  const app = await NestFactory.create<NestExpressApplication>(AppModule, appOptions);

  Sentry.init({
    dsn: process.env.SENTRY_DSN,
    // release: `openpublicatie-api@${p.version}`,
    environment: process.env.NODE_ENV
  });
  app.useGlobalInterceptors(new SentryInterceptor());
  
  app.use(helmet());
  app.setGlobalPrefix('v1');

  // Handle class-validator response 
  // 
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    transform: true,
    exceptionFactory: (errors: ValidationError[]) => {
      const a:any =[]; 
      errors.map((error: any) => {
        a.push({
          field: error.property,
          message: Object.values(error.constraints)
        });
      });

      return new BadRequestException(a);
    }
  }));

  await app.listen(process.env.PORT);
}

bootstrap();
