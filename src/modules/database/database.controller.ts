import { Controller } from '@nestjs/common';
import { DatabaseService } from './database.service';

@Controller('database')
export class DatabaseController {
  constructor(private databaseService: DatabaseService) {
    this.initDB();
  }

  initDB = async () => {
    try {
      await this.databaseService.runMigrations();
    } catch (err) {
      console.warn(err);
    }
  };
}
