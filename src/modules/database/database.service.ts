import { Inject, Injectable } from '@nestjs/common';
import { Connection, Migration, Repository } from 'typeorm';

@Injectable()
export class DatabaseService {
  constructor(
    @Inject('Connection')
    public connection: Connection,
  ) {}

  async dropDatabase(): Promise<void> {
    return await this.connection.dropDatabase();
  }

  async synchronizeDatabase(): Promise<void> {
    return await this.connection.synchronize();
  }

  async runMigrations(): Promise<Migration[]> {
    return await this.connection.runMigrations();
  }

  async getRepository<T>(entity: any): Promise<Repository<T>> {
    return this.connection.getRepository(entity);
  }
}
