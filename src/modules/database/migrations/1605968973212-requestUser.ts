import {MigrationInterface, QueryRunner} from "typeorm";

export class requestUser1605968973212 implements MigrationInterface {
    name = 'requestUser1605968973212'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" ADD "user_id" integer`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "FK_3a3d93f532a056b0d89d09cdd21" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "FK_3a3d93f532a056b0d89d09cdd21"`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "user_id"`);
    }

}
