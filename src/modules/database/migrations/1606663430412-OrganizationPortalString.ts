import {MigrationInterface, QueryRunner} from "typeorm";

export class OrganizationPortalString1606663430412 implements MigrationInterface {
    name = 'OrganizationPortalString1606663430412'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "portal"`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "portal" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "portal"`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "portal" boolean`);
    }

}
