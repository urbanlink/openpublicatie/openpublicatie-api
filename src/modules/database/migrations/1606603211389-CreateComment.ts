import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateComment1606603211389 implements MigrationInterface {
    name = 'CreateComment1606603211389'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "comment" ("id" SERIAL NOT NULL, "body" text, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "requestId" integer, "userId" integer, CONSTRAINT "REL_66c955fd4ac8c4ecb158817ef6" UNIQUE ("requestId"), CONSTRAINT "REL_c0354a9a009d3bb45a08655ce3" UNIQUE ("userId"), CONSTRAINT "PK_0b0e4bbc8415ec426f87f3a88e2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_66c955fd4ac8c4ecb158817ef6f" FOREIGN KEY ("requestId") REFERENCES "request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_c0354a9a009d3bb45a08655ce3b" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_c0354a9a009d3bb45a08655ce3b"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_66c955fd4ac8c4ecb158817ef6f"`);
        await queryRunner.query(`DROP TABLE "comment"`);
    }

}
