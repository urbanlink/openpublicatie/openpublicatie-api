import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateRequestState1606678746058 implements MigrationInterface {
    name = 'UpdateRequestState1606678746058'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."request_status_enum" RENAME TO "request_status_enum_old"`);
        await queryRunner.query(`CREATE TYPE "request_status_enum" AS ENUM('0', '1', '2', '3', '4', '5', '6', '7', '8')`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" TYPE "request_status_enum" USING "status"::"text"::"request_status_enum"`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" SET DEFAULT '0'`);
        await queryRunner.query(`DROP TYPE "request_status_enum_old"`);
        await queryRunner.query(`COMMENT ON COLUMN "request"."status" IS NULL`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" SET DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" SET DEFAULT 'concept'`);
        await queryRunner.query(`COMMENT ON COLUMN "request"."status" IS NULL`);
        await queryRunner.query(`CREATE TYPE "request_status_enum_old" AS ENUM('concept', 'Klaar voor verzending', 'Wachtend op bevestiging', 'Wachtend op antwoord', 'Verbetering vereist', 'Afgewezen', 'Incompleet', 'Agerond', 'Ingetrokken')`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" TYPE "request_status_enum_old" USING "status"::"text"::"request_status_enum_old"`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" SET DEFAULT '0'`);
        await queryRunner.query(`DROP TYPE "request_status_enum"`);
        await queryRunner.query(`ALTER TYPE "request_status_enum_old" RENAME TO  "request_status_enum"`);
    }

}
