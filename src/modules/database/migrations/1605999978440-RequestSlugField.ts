import {MigrationInterface, QueryRunner} from "typeorm";
import { v4 as uuidv4 } from 'uuid';

export class RequestSlugField1605999978440 implements MigrationInterface {
    name = 'RequestSlugField1605999978440'

    createslug = () => {
        return uuidv4();
    }
    public async up(queryRunner: QueryRunner): Promise<void> {
        
        await queryRunner.query(`ALTER TABLE "request" ADD "slug" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "UQ_b0dfdb303924b1b2a4a557ffd23" UNIQUE ("slug")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "UQ_b0dfdb303924b1b2a4a557ffd23"`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "slug"`);
    }

}
