import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCommunication1606606765449 implements MigrationInterface {
    name = 'AddCommunication1606606765449'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "FK_d17376b62eff59788718fea9966"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_66c955fd4ac8c4ecb158817ef6f"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_c0354a9a009d3bb45a08655ce3b"`);
        await queryRunner.query(`CREATE TABLE "communication" ("id" SERIAL NOT NULL, "subject" character varying NOT NULL, "body" text, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "request_id" integer, CONSTRAINT "PK_392407b9e9100bee1a64e26cd5d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "notification" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" text, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_705b6c7cdf9b2c2ff7ac7872cb7" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "organization_id"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "REL_66c955fd4ac8c4ecb158817ef6"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "requestId"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "REL_c0354a9a009d3bb45a08655ce3"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "userId"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "request_id" integer`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "user_id" integer`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "UQ_bbfe153fa60aa06483ed35ff4a7" UNIQUE ("user_id")`);
        await queryRunner.query(`ALTER TABLE "communication" ADD CONSTRAINT "FK_637cb7e14e0799bddcd81902ee7" FOREIGN KEY ("request_id") REFERENCES "request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_67f4272c51efb44e3fd23db9d2e" FOREIGN KEY ("request_id") REFERENCES "request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_bbfe153fa60aa06483ed35ff4a7" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_bbfe153fa60aa06483ed35ff4a7"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "FK_67f4272c51efb44e3fd23db9d2e"`);
        await queryRunner.query(`ALTER TABLE "communication" DROP CONSTRAINT "FK_637cb7e14e0799bddcd81902ee7"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP CONSTRAINT "UQ_bbfe153fa60aa06483ed35ff4a7"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "comment" DROP COLUMN "request_id"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "userId" integer`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "REL_c0354a9a009d3bb45a08655ce3" UNIQUE ("userId")`);
        await queryRunner.query(`ALTER TABLE "comment" ADD "requestId" integer`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "REL_66c955fd4ac8c4ecb158817ef6" UNIQUE ("requestId")`);
        await queryRunner.query(`ALTER TABLE "request" ADD "organization_id" integer`);
        await queryRunner.query(`DROP TABLE "notification"`);
        await queryRunner.query(`DROP TABLE "communication"`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_c0354a9a009d3bb45a08655ce3b" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "comment" ADD CONSTRAINT "FK_66c955fd4ac8c4ecb158817ef6f" FOREIGN KEY ("requestId") REFERENCES "request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "FK_d17376b62eff59788718fea9966" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
