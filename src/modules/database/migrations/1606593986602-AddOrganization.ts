import {MigrationInterface, QueryRunner} from "typeorm";

export class AddOrganization1606593986602 implements MigrationInterface {
    name = 'AddOrganization1606593986602'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" ADD "organization_id" integer`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "FK_d17376b62eff59788718fea9966" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "FK_d17376b62eff59788718fea9966"`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "organization_id"`);
    }

}
