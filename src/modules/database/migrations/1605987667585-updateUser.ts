import {MigrationInterface, QueryRunner} from "typeorm";

export class updateUser1605987667585 implements MigrationInterface {
    name = 'updateUser1605987667585'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "email_verified"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "email_verified" boolean NOT NULL`);
    }

}
