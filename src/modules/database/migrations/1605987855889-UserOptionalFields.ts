import {MigrationInterface, QueryRunner} from "typeorm";

export class UserOptionalFields1605987855889 implements MigrationInterface {
    name = 'UserOptionalFields1605987855889'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "signed_up"`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "slug" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."slug" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "picture" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."picture" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "account_plan" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."account_plan" IS NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "user"."account_plan" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "account_plan" SET NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."picture" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "picture" SET NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "user"."slug" IS NULL`);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "slug" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "user" ADD "signed_up" TIMESTAMP WITH TIME ZONE`);
    }

}
