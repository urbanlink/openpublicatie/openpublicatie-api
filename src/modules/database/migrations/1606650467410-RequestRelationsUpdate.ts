import {MigrationInterface, QueryRunner} from "typeorm";

export class RequestRelationsUpdate1606650467410 implements MigrationInterface {
    name = 'RequestRelationsUpdate1606650467410'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "notification" ADD "request_id" integer`);
        await queryRunner.query(`ALTER TABLE "publication" ADD "request_id" integer`);
        await queryRunner.query(`ALTER TABLE "notification" ADD CONSTRAINT "FK_5878f4b35143abceccd77125177" FOREIGN KEY ("request_id") REFERENCES "request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "publication" ADD CONSTRAINT "FK_d116d8a5f632dc2c0ba90c0a1ed" FOREIGN KEY ("request_id") REFERENCES "request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "publication" DROP CONSTRAINT "FK_d116d8a5f632dc2c0ba90c0a1ed"`);
        await queryRunner.query(`ALTER TABLE "notification" DROP CONSTRAINT "FK_5878f4b35143abceccd77125177"`);
        await queryRunner.query(`ALTER TABLE "publication" DROP COLUMN "request_id"`);
        await queryRunner.query(`ALTER TABLE "notification" DROP COLUMN "request_id"`);
    }

}
