import {MigrationInterface, QueryRunner} from "typeorm";

export class AddCommunicationPublication1606644228334 implements MigrationInterface {
    name = 'AddCommunicationPublication1606644228334'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "publication" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" text, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_8aea8363d5213896a78d8365fab" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "notification" DROP COLUMN "title"`);
        await queryRunner.query(`ALTER TABLE "notification" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "communication" ADD "send_date" TIMESTAMP WITH TIME ZONE`);
        await queryRunner.query(`ALTER TABLE "notification" ADD "message" text`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "notification" DROP COLUMN "message"`);
        await queryRunner.query(`ALTER TABLE "communication" DROP COLUMN "send_date"`);
        await queryRunner.query(`ALTER TABLE "notification" ADD "description" text`);
        await queryRunner.query(`ALTER TABLE "notification" ADD "title" character varying NOT NULL`);
        await queryRunner.query(`DROP TABLE "publication"`);
    }

}
