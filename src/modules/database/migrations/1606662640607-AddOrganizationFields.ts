import {MigrationInterface, QueryRunner} from "typeorm";

export class AddOrganizationFields1606662640607 implements MigrationInterface {
    name = 'AddOrganizationFields1606662640607'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "organization" ADD "slug" character varying`);
        await queryRunner.query(`ALTER TABLE "organization" ADD CONSTRAINT "UQ_a08804baa7c5d5427067c49a31f" UNIQUE ("slug")`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "verified" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "logo" character varying`);
        await queryRunner.query(`CREATE TYPE "organization_level_enum" AS ENUM('Gemeente', 'Provincie', 'Waterschap', 'Ministerie', 'Bedrijf', 'Stichting')`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "level" "organization_level_enum" NOT NULL DEFAULT 'Gemeente'`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "url" character varying`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "twitter" character varying`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "portal" boolean`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "email" character varying`);
        await queryRunner.query(`ALTER TABLE "organization" ADD "address" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "address"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "email"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "portal"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "twitter"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "url"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "level"`);
        await queryRunner.query(`DROP TYPE "organization_level_enum"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "logo"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "verified"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP CONSTRAINT "UQ_a08804baa7c5d5427067c49a31f"`);
        await queryRunner.query(`ALTER TABLE "organization" DROP COLUMN "slug"`);
    }

}
