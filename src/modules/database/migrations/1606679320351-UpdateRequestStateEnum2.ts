import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateRequestStateEnum21606679320351 implements MigrationInterface {
    name = 'UpdateRequestStateEnum21606679320351'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TYPE "public"."request_status_enum" RENAME TO "request_status_enum_old"`);
        await queryRunner.query(`CREATE TYPE "request_status_enum" AS ENUM('concept', 'ready_for_sending', 'awaiting_confirmation', 'awaiting_answer', 'improvement_needed', 'declined', 'incomplete', 'finished', 'abandoned')`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" TYPE "request_status_enum" USING "status"::"text"::"request_status_enum"`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" SET DEFAULT 'concept'`);
        await queryRunner.query(`DROP TYPE "request_status_enum_old"`);
        await queryRunner.query(`COMMENT ON COLUMN "request"."status" IS NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "request"."status" IS NULL`);
        await queryRunner.query(`CREATE TYPE "request_status_enum_old" AS ENUM('concept', 'Klaar voor verzending', 'Wachtend op bevestiging', 'Wachtend op antwoord', 'Verbetering vereist', 'Afgewezen', 'Incompleet', 'Agerond', 'Ingetrokken')`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" TYPE "request_status_enum_old" USING "status"::"text"::"request_status_enum_old"`);
        await queryRunner.query(`ALTER TABLE "request" ALTER COLUMN "status" SET DEFAULT 'concept'`);
        await queryRunner.query(`DROP TYPE "request_status_enum"`);
        await queryRunner.query(`ALTER TYPE "request_status_enum_old" RENAME TO  "request_status_enum"`);
    }

}
