import {MigrationInterface, QueryRunner} from "typeorm";

export class RequestOrganization1606649554281 implements MigrationInterface {
    name = 'RequestOrganization1606649554281'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "FK_3a3d93f532a056b0d89d09cdd21"`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "user_id"`);
        await queryRunner.query(`ALTER TABLE "request" ADD "owner_id" integer`);
        await queryRunner.query(`ALTER TABLE "request" ADD "organization_id" integer`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "FK_0dc0d8afc0a15fb6c273f72af6a" FOREIGN KEY ("owner_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "FK_d17376b62eff59788718fea9966" FOREIGN KEY ("organization_id") REFERENCES "organization"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "FK_d17376b62eff59788718fea9966"`);
        await queryRunner.query(`ALTER TABLE "request" DROP CONSTRAINT "FK_0dc0d8afc0a15fb6c273f72af6a"`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "organization_id"`);
        await queryRunner.query(`ALTER TABLE "request" DROP COLUMN "owner_id"`);
        await queryRunner.query(`ALTER TABLE "request" ADD "user_id" integer`);
        await queryRunner.query(`ALTER TABLE "request" ADD CONSTRAINT "FK_3a3d93f532a056b0d89d09cdd21" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
