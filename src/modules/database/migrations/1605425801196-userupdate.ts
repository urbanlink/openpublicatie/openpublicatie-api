import {MigrationInterface, QueryRunner} from "typeorm";

export class userupdate1605425801196 implements MigrationInterface {
    name = 'userupdate1605425801196'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "refresh_token" ("id" SERIAL NOT NULL, "value" character varying NOT NULL, "expiresAt" TIMESTAMP NOT NULL, "clientId" character varying NOT NULL, "ipAddress" character varying NOT NULL, CONSTRAINT "UQ_7f2bc25df3afe0d69f71bd61705" UNIQUE ("value"), CONSTRAINT "PK_b575dd3c21fb0831013c909e7fe" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "organization" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" text, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_472c1f99a32def1b0abb219cd67" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TYPE "request_status_enum" AS ENUM('concept', 'Klaar voor verzending', 'Wachtend op bevestiging', 'Wachtend op antwoord', 'Verbetering vereist', 'Afgewezen', 'Incompleet', 'Agerond', 'Ingetrokken')`);
        await queryRunner.query(`CREATE TABLE "request" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" text, "status" "request_status_enum" NOT NULL DEFAULT 'concept', "date_filed" TIMESTAMP WITH TIME ZONE, "response_deadline" TIMESTAMP WITH TIME ZONE, "date_finished" TIMESTAMP WITH TIME ZONE, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "PK_167d324701e6867f189aed52e18" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "auth0" character varying NOT NULL, "email" character varying NOT NULL, "email_verified" boolean NOT NULL, "nickname" character varying NOT NULL, "slug" character varying NOT NULL, "picture" character varying NOT NULL, "account_plan" character varying NOT NULL, "account_plan_end" TIMESTAMP WITH TIME ZONE, "admin" boolean NOT NULL DEFAULT false, "bio" text, "firstname" character varying, "lastname" character varying, "signed_up" TIMESTAMP WITH TIME ZONE, "notification_schedule" integer DEFAULT '1440', "last_email_notification" TIMESTAMP, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_53b6c937eeb6fe1ded176b55907" UNIQUE ("auth0"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "UQ_e2364281027b926b879fa2fa1e0" UNIQUE ("nickname"), CONSTRAINT "UQ_ac08b39ccb744ea6682c0db1c2d" UNIQUE ("slug"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "request"`);
        await queryRunner.query(`DROP TYPE "request_status_enum"`);
        await queryRunner.query(`DROP TABLE "organization"`);
        await queryRunner.query(`DROP TABLE "refresh_token"`);
    }

}
