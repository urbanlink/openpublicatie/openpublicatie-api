import { IsNumber, IsOptional, IsString, Length } from 'class-validator';
import { Organization } from 'src/modules/organization/organization.entity';

export class UpdateRequestDto {
  @IsNumber()
  public id: number;

  @IsString()
  @Length(3, 150)
  public title: string;

  @IsString()
  public description: string;

  @IsOptional()
  organization: Organization
}
