import { IsDate, IsEnum, IsOptional, IsString, MaxLength } from 'class-validator';
import { Organization } from 'src/modules/organization/organization.entity';
import { User } from 'src/modules/user/user.entity';
import { RequestStatus } from '../request.entity';

export class CreateRequestDto {

  @IsString()
  title: string;
  
  @IsString()
  @IsOptional()
  slug: string;

  @IsString()
  @MaxLength(500)
  description: string;

  @IsOptional()
  @IsEnum(RequestStatus)
  status: string

  @IsDate()
  @IsOptional()
  date_filed: Date

  @IsDate()
  @IsOptional()
  response_deadline: Date

  @IsDate()
  @IsOptional()
  date_finished: Date

  @IsOptional()
  owner: User
  
  @IsOptional()
  organization: Organization
}
