import { Body, Controller, Delete, Get, Logger, Param, Put, UseGuards, Post, Req, HttpException, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from '../auth/auth.guards';
import { CreateRequestDto } from './dto/createRequest.dto';
import { UpdateRequestDto } from './dto/updateRequest.dto';
import { RequestService } from './request.service';
import { Permissions } from '../auth/permissions.decorator';
import { Request as RequestModel} from './request.entity';
import { AuthService } from '../auth/auth.service';
import slug from 'slug';

@Controller('request')
export class RequestController {
  private readonly logger = new Logger(RequestController.name);

  constructor(
    private readonly requestService: RequestService,
    private readonly authService: AuthService
  ) {}

  @Get()
  async findAll(@Param('take') take: number, @Param('skip') skip: number): Promise<RequestModel[]> {
    skip = skip || 0; 
    take = take || 100; 
    
    return this.requestService.findAll({take, skip});
  }

  @Get(':slug')
  async find(@Param('slug') slug: string): Promise<RequestModel> {
    return this.requestService.find(slug);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Post()
  @Permissions('create:requests')
  async create(@Body('request') data: CreateRequestDto, @Req() req: any) {
    this.logger.log('Create new request', JSON.stringify(data)); 
    // Create slug 
    let s = slug(data.title, { lower: true }); 
    s = s.substring(0, 10);
    s = s + '-' + Math.random().toString(36).substr(2, 5);
    data.slug = s; 

    // Add owner 
    try {
      data.owner = await this.authService.currentUser(req.user.sub);
    } catch (err) {
      throw new HttpException('Email not verified', HttpStatus.FORBIDDEN);
    }
    
    // Create the item
    return this.requestService.create(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Put(':id')
  @Permissions('update:requests')
  async update(@Param() id: number, @Body() data: UpdateRequestDto) {
    return this.requestService.update(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:requests')
  async delete(@Param('id') id: number) {
    return this.requestService.delete(id);
  }
}
