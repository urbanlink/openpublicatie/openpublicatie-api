
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn } from 'typeorm';
import { Comment } from '../comment/comment.entity';
import { Communication } from '../communication/communication.entity';
import { Notification } from '../notification/notification.entity';
import { Organization } from '../organization/organization.entity';
import { Publication } from '../publication/publication.entity';
import { User } from '../user/user.entity';

export enum RequestStatus {
  CONCEPT = 'concept',
  READY_FOR_SENDING = 'ready_for_sending',
  AWAITING_CONFIRMATION = 'awaiting_confirmation',
  AWAITING_ANSWER = 'awaiting_answer',
  IMPROVEMENT_NEEDED = 'improvement_needed',
  DECLINED = 'declined',
  INCOMPLETE = 'incomplete',
  FINISHED = 'finished',
  ABANDONED = 'abandoned'
}

@Entity()
export class Request {

  @PrimaryGeneratedColumn()
    readonly id: number;

  @Column()
  title: string;
  
  @Column({unique: true})
  slug: string;

  @Column({
    type: 'text',
    nullable: true
  })
    description: string;

  @Column({
    type: 'enum',
    enum: RequestStatus,
    default: RequestStatus.CONCEPT
  })
   status: string

  @Column({
    type: 'timestamptz',
    nullable: true
  })
    date_filed: Date

  @Column({
    type: 'timestamptz',
    nullable: true
  })
    response_deadline: Date

  @Column({
    type: 'timestamptz',
    nullable: true
  })
    date_finished: Date

  @CreateDateColumn({
      name: 'created_at',
      type: 'timestamptz'
    })
    created_at: Date

  @UpdateDateColumn({
      name: 'updated_at',
      type: 'timestamptz'
    })
    updated_at: Date

  // Request owner
  @ManyToOne(() => User, user => user.requests)
  @JoinColumn({ name: 'owner_id' })
  owner: User;
  
  // Request organization
  @ManyToOne(() => Organization, organization => organization.requests)
  @JoinColumn({ name: 'organization_id' })
  organization: Organization;

  @OneToMany(() => Communication, communication => communication.request)
  communications: Communication[];

  @OneToMany(() => Comment, comment =>comment.request)
  comments: Comment[];

  @OneToMany(() => Notification, notification => notification.request)
  notifications: Notification[];

  @OneToMany(() => Publication, publication => publication.request)
  publications: Publication[];

}

export default Request;
