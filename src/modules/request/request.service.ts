import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindManyOptions, Repository } from 'typeorm';
import { Request } from './request.entity';
import { CreateRequestDto } from './dto/createRequest.dto';
import { UpdateRequestDto } from './dto/updateRequest.dto';

@Injectable()
export class RequestService {
  private readonly logger = new Logger(RequestService.name);

  constructor(
    @InjectRepository(Request)
    private requestRepository: Repository<Request>,
  ) {}


  async findAll(options?: FindManyOptions): Promise<Request[]> {
    // const { take, skip } = options; 
    console.log(options)
    const requests = await this.requestRepository.find(
      {
        relations: ['organization']
      }
    );
    if (requests) return requests;

    throw new HttpException('No requests found', HttpStatus.NOT_FOUND);
  }

  async find(slug: string): Promise<Request> {
    const request = await this.requestRepository.findOne({
      where: { slug },
      relations: [
        'owner',
        'organization',
        'comments',
        'communications',
        'notifications',
        'publications'
      ]
    });
    if (request) return request;

    throw new HttpException('No request found', HttpStatus.NOT_FOUND);
  }

  async create(data: CreateRequestDto): Promise<Request> {
    const newRequest = this.requestRepository.create(data);
    return await this.requestRepository.save(newRequest);
  }

  async update(data: UpdateRequestDto): Promise<Request> {
    const { id } = data;
    const record = await this.requestRepository.findOne({ where: { id } });
    if (!record) throw new HttpException('No requests found', HttpStatus.NOT_FOUND);

    return await this.requestRepository.save({
      ...record,
      ...data,
    });
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.requestRepository.delete(id);
  }
}
