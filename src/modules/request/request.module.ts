import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import Request from './request.entity';
import { RequestController } from './request.controller';
import { RequestService } from './request.service';
import { User } from '../user/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Request, User]), AuthModule],
  controllers: [RequestController],
  providers: [RequestService],
  exports: [RequestService],
})
export class RequestModule {}
