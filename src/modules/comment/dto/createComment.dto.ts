import {  IsNumber, IsOptional, IsString } from 'class-validator';
import { User } from 'src/modules/user/user.entity';
import { Request } from 'src/modules/request/request.entity';

export class CreateCommentDto {

  // @IsOptional()
  // @IsString()
  // subject: string;
  
  @IsString()
  body: string;

  @IsNumber()
  request: Request

  // @IsNumber()
  // @IsOptional()
  // parent: Comment

  @IsNumber()
  @IsOptional()
  user: User

}
