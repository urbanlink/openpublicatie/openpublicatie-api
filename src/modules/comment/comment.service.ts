import { HttpException, HttpStatus, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Repository } from "typeorm";
import { Comment } from "./comment.entity";
import { CreateCommentDto } from './dto/createComment.dto';

@Injectable()
export class CommentService {
  private readonly logger = new Logger(CommentService.name);

  constructor(
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>
  ) {}

  async findAll(): Promise<Comment[]> {
    const comments = await this.commentRepository.find();
    if (comments) return comments;

    throw new HttpException('No comments found', HttpStatus.NOT_FOUND);
  }

  async create(data: CreateCommentDto): Promise<Comment> {
    const newComment = this.commentRepository.create(data);

    return await this.commentRepository.save(newComment);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.commentRepository.delete(id);
  }
}
