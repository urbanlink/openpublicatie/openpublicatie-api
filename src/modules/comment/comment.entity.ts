import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn 
} from 'typeorm';
import { Request } from '../request/request.entity';
import { User } from '../user/user.entity';


@Entity()
export class Comment {

  @PrimaryGeneratedColumn()
  readonly id: number;

  // Fields 
  // @Column()
  // subject: string;

  @Column({
    type: 'text',
    nullable: true
  })
  body: string;
  
  // Created, updated
  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  created_at: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  updated_at: Date

  // Relations
  @ManyToOne(() => Request, request => request.comments)
  @JoinColumn({name:'request_id'})
  request: Request;

  @OneToOne(() => User)
  @JoinColumn({name:'user_id'})
  user: User;

  // @OneToOne(() => Comment)
  // @JoinColumn()
  // parent: Comment;

}
