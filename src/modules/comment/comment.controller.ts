import {
  Body,
  Controller, Delete, Get, Logger, Param, UseGuards,
  Post, Req, HttpException, HttpStatus
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from '../auth/auth.guards';
import { CreateCommentDto } from './dto/createComment.dto';
import { Permissions } from '../auth/permissions.decorator';
import { CommentService } from './comment.service';
import { Comment } from './comment.entity';
import { AuthService } from '../auth/auth.service';

@Controller('comment')
export class CommentController {
  private readonly logger = new Logger(CommentController.name);

  constructor(private readonly commentService: CommentService, private readonly authService: AuthService) {}

  @Get()
  async findAll(): Promise<Comment[]> {
    return this.commentService.findAll();
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Post()
  @Permissions('create:comment')
  async create(@Req() req:any, @Body('comment') data: CreateCommentDto) {

    // Add owner 
    try {
      data.user = await this.authService.currentUser(req.user.sub);
    } catch (err) {
      throw new HttpException('User not found', HttpStatus.FORBIDDEN);
    }

    return this.commentService.create(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:comments')
  async delete(@Param('id') id: number) {
    return this.commentService.delete(id);
  }
}
