import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from '@nestjs/config';

import { RefreshToken } from './refreshToken.entity';
import { UserModule } from '../user/user.module';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { TokenService } from './refreshToken.service';
import { AuthService } from './auth.service';
import { User } from '../user/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([RefreshToken, User]),
    UserModule,
    PassportModule,
    ConfigModule, 
    UserModule
  ],
  providers: [LocalStrategy, JwtStrategy, TokenService, AuthService],
  exports: [AuthService]
})
export class AuthModule {}
