import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/user.entity';

import { ManagementClient } from 'auth0'; 
import { CreateUserDto } from '../user/dto/createUser.dto';

const auth0Manager = new ManagementClient({
  domain: 'openpub.eu.auth0.com',
  clientId: 'KZ8b210SvFj7u1fqdO4tU2PBxF8lBLrL',
  clientSecret: 'CYu4bYKgmwJ0RodVYMyT36ELFQYjFTNvI1mE7GVmrdI90Uvhvz59ZjH-rqT9yacX',
  scope: 'read:users update:users'
});

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) { }

  /**
   * 
   * Fetch current user based on auth0 sub 
   * Create new user when local user does not exist 
   * 
   **/ 
  async currentUser(sub: string): Promise<User> {
    try {
      const user = await this.userRepository.findOne({ where: { auth0: sub } });
      if (user) return user;

      // No user yet in app database, fetch user from auth0 and save 
      this.logger.log('Eser not found in db. Fetch profile from auth0');
      const auth0User = await auth0Manager.getUser({ id: sub }); 
      // Validate if user exists and email is verified 
      if (!auth0User || !auth0User.user_id) throw new HttpException('No profile received from Auth provider', HttpStatus.SERVICE_UNAVAILABLE);
      if (!auth0User.email_verified) throw new HttpException('Email not verified', HttpStatus.SERVICE_UNAVAILABLE);
      
      // Save user in db 
      const newUserData: CreateUserDto = {
        email: auth0User.email,
        nickname: auth0User.nickname,
        picture: auth0User.picture,
        auth0: auth0User.user_id,        
      }
      const newUser = this.userRepository.create(newUserData);
      return await this.userRepository.save(newUser);
      
    } catch (err) {
      throw new HttpException(err, HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

}