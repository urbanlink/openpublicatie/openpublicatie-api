import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy as BaseStrategy } from 'passport-jwt';
import { passportJwtSecret } from 'jwks-rsa';

// import { UserService } from '../../user/user.service';
import { IJwtPayload } from '../auth.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(BaseStrategy) {
  constructor() {
    super({
      secretOrKeyProvider: passportJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `${process.env.AUTH0_ISSUER_URL}.well-known/jwks.json`,
      }),
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      audience: process.env.AUTH0_AUDIENCE,
      issuer: process.env.AUTH0_ISSUER_URL,
      algorithms: ['RS256'],
    });
  }

  validate(payload: IJwtPayload): IJwtPayload {
    const minimumScope = ['openid', 'profile'];
    if (payload?.scope?.split(' ').filter((scope) => minimumScope.indexOf(scope) > -1).length !== minimumScope.length) {
      throw new UnauthorizedException('JWT does not possess the required scope (`openid profile email`).');
    }

    return payload;
  }
}
