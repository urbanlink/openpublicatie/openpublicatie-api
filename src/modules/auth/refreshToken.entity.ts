import {
  Entity, PrimaryGeneratedColumn, Column,
  // ManyToOne
} from 'typeorm';
// import { User } from '../user/user.entity';

@Entity()
export class RefreshToken {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ unique: true })
  value: string;

  // @ManyToOne(() => User, (user) => user.refreshTokens)
  // public user: User;

  @Column()
  public expiresAt: Date;

  @Column()
  public clientId: string;

  @Column()
  public ipAddress: string;
}
