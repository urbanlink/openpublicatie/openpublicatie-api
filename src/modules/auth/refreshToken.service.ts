import { Injectable, Logger, HttpException, HttpStatus } from '@nestjs/common';
import { randomBytes } from 'crypto';
import { sign, SignOptions, verify } from 'jsonwebtoken';
import dayjs from 'dayjs';
import { v4 as uuidv4 } from 'uuid';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RefreshToken } from './refreshToken.entity';
import { UserService } from '../user/user.service';
import { IJwtPayload, ILoginResponse } from './auth.interface';

@Injectable()
export class TokenService {
  private readonly logger = new Logger(TokenService.name);

  private readonly jwtOptions: SignOptions;
  private readonly jwtKey: string;
  private refreshTokenTtl: number;
  private expiresInDefault: string | number;

  // @todo: should be put in redis cache
  private readonly usersExpired: number[] = [];

  constructor(
    @InjectRepository(RefreshToken)
    private refreshTokenRepository: Repository<RefreshToken>,
    private userService: UserService,
    private readonly configService: ConfigService,
  ) {
    this.expiresInDefault = this.configService.get('JWT_ACCESSTOKEN_TTL');
    this.jwtOptions = { expiresIn: this.expiresInDefault };
    this.jwtKey = this.configService.get('JWT_KEY');
    this.refreshTokenTtl = this.configService.get('JWT_REFRESHTOKEN_TTL');
  }

  async getAccessTokenFromRefreshToken(
    refreshToken: string,
    oldAccessToken: string,
    clientId: string,
    ipAddress: string,
  ): Promise<ILoginResponse> {
    try {
      // check if refresh token exist in database
      const token = await this.refreshTokenRepository.findOne({ where: { value: refreshToken } });
      const currentDate = new Date();
      if (!token) {
        throw new HttpException('Refresh token not found', HttpStatus.NOT_FOUND);
      }
      if (token.expiresAt < currentDate) {
        throw new HttpException('Refresh token expired', HttpStatus.NOT_ACCEPTABLE);
      }
      // Refresh token is still valid
      // Generate new access token
      const oldPayload = await this.validateToken(oldAccessToken, true);
      const payload = {
        sub: oldPayload.sub,
      };
      const accessToken = await this.createAccessToken(payload);
      // Remove old refresh token and generate a new one
      await this.refreshTokenRepository.delete(token);

      accessToken.refreshToken = await this.createRefreshToken({
        userId: oldPayload.sub,
        clientId,
        ipAddress,
      });

      return accessToken;
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  async createAccessToken(payload: IJwtPayload, expires = this.expiresInDefault): Promise<ILoginResponse> {
    // If expires is negative it means that token should not expire
    const options = this.jwtOptions;
    expires > 0 ? (options.expiresIn = expires) : delete options.expiresIn;
    // Generate unique id for this token
    options.jwtid = uuidv4();
    const signedPayload = sign(payload, this.jwtKey, options);
    const token: ILoginResponse = {
      accessToken: signedPayload,
      expiresIn: expires,
    };

    return token;
  }

  async createRefreshToken(tokenContent: { userId: string; clientId: string; ipAddress: string }): Promise<string> {
    const {  clientId, ipAddress } = tokenContent;

    const token = this.refreshTokenRepository.create();
    // const user = await this.userService.getById(Number(userId));
    const refreshToken = randomBytes(64).toString('hex');

    // token.user = user;
    token.value = refreshToken;
    token.clientId = clientId;
    token.ipAddress = ipAddress;
    token.expiresAt = dayjs().add(this.refreshTokenTtl, 'd').toDate();

    await this.refreshTokenRepository.create(token);

    return refreshToken;
  }

  /**
   * Remove all the refresh tokens associated to a user
   * @param userId id of the user
   */
  async deleteRefreshTokenForUser(userId: string) {
    // const user = await this.userService.getById(Number(userId));
    // await this.refreshTokenRepository.delete({ user });
    await this.revokeTokenForUser(userId);
  }

  /**
   * Removes a refresh token, and invalidated all access tokens for the user
   * @param userId id of the user
   * @param value the value of the token to remove
   */
  async deleteRefreshToken(userId: string, value: string) {
    await this.refreshTokenRepository.delete({ value });
    await this.revokeTokenForUser(userId);
  }

  async decodeAndValidateJWT(token: string): Promise<any> {
    if (token) {
      try {
        const payload = await this.validateToken(token);
        return await this.validatePayload(payload);
      } catch (error) {
        return null;
      }
    }
  }

  async validatePayload(payload: IJwtPayload): Promise<any> {
    const tokenBlacklisted = await this.isBlackListed(payload.sub, payload.exp);
    if (!tokenBlacklisted) {
      return {
        id: payload.sub,
      };
    }
    return null;
  }

  private async validateToken(token: string, ignoreExpiration = false): Promise<IJwtPayload> {
    return verify(token, this.configService.get('JWT_KEY'), {
      ignoreExpiration,
    }) as IJwtPayload;
  }

  private async isBlackListed(id: string, expire: number): Promise<boolean> {
    const u = this.usersExpired.filter((val) => expire < val);
    return u && u.length > 0;
    // return this.usersExpired[id] && expire < this.usersExpired[id];
  }

  private async revokeTokenForUser(userId: string): Promise<any> {
    this.usersExpired[Number(userId)] = dayjs().add(Number(this.expiresInDefault), 's').unix();
  }
}
