import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';
// import { UserService } from 'src/user/user.service';
// import { AuthService } from './auth.service';
// import { ExtractJwt } from 'passport-jwt';

//
@Injectable()
export class JwtAuthenticationGuard extends AuthGuard('jwt') {
  // The following ix an implementation if you use cookies. Otherwse the frontend renews the token by sending a request for renewal.
  // https://stackoverflow.com/questions/53296157/how-to-refresh-token-in-nestjs
  // private logger = new Logger(JwtAuthenticationGuard.name);
  // constructor(private readonly authService: AuthService, private readonly userService: UserService) {
  //   super();
  // }
  // async canActivate(context: ExecutionContext): Promise<boolean> {
  //   const request = context.switchToHttp().getRequest();
  //   const response = context.switchToHttp().getResponse();
  //   try {
  //     const accessToken = ExtractJwt.fromAuthHeaderAsBearerToken();
  //     if (!accessToken) throw new UnauthorizedException('Access token is not set');
  //     const isValidAccessToken = this.authService.validateToken(accessToken);
  //     if (isValidAccessToken) return this.activate(context);
  //     const refreshToken = request.cookies[REFRESH_TOKEN_COOKIE_NAME];
  //     if (!refreshToken) throw new UnauthorizedException('Refresh token is not set');
  //     const isValidRefreshToken = this.authService.validateToken(refreshToken);
  //     if (!isValidRefreshToken) throw new UnauthorizedException('Refresh token is not valid');
  //     const user = await this.userService.getByRefreshToken(refreshToken);
  //     const { accessToken: newAccessToken, refreshToken: newRefreshToken } = this.authService.createTokens(user.id);
  //     await this.userService.updateRefreshToken(user.id, newRefreshToken);
  //     request.cookies[ACCESS_TOKEN_COOKIE_NAME] = newAccessToken;
  //     request.cookies[REFRESH_TOKEN_COOKIE_NAME] = newRefreshToken;
  //     response.cookie(ACCESS_TOKEN_COOKIE_NAME, newAccessToken, COOKIE_OPTIONS);
  //     response.cookie(REFRESH_TOKEN_COOKIE_NAME, newRefreshToken, COOKIE_OPTIONS);
  //     return this.activate(context);
  //   } catch (err) {
  //     this.logger.error(err.message);
  //     response.clearCookie(ACCESS_TOKEN_COOKIE_NAME, COOKIE_OPTIONS);
  //     response.clearCookie(REFRESH_TOKEN_COOKIE_NAME, COOKIE_OPTIONS);
  //     return false;
  //   }
  // }
  // async activate(context: ExecutionContext): Promise<boolean> {
  //   return super.canActivate(context) as Promise<boolean>;
  // }
  // handleRequest(err, user) {
  //   if (err || !user) {
  //     throw new UnauthorizedException();
  //   }
  //   return user;
  // }
}

//
@Injectable()
export class LocalAuthenticationGuard extends AuthGuard('local') {}

//
@Injectable()
export class PermissionsGuard implements CanActivate {
  
  constructor(private readonly reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    
    const routePermissions = this.reflector.get<string[]>('permissions', context.getHandler());
    const userPermissions = context.getArgs()[0].user.permissions;

    if (!routePermissions) return true;

    const hasPermission = () => routePermissions.every((routePermission) => userPermissions.includes(routePermission));

    return hasPermission();
  }
}
