import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Email from 'email-templates';
import { SentMessageInfo } from 'nodemailer';
import path from 'path';
import { ISendMailOptions } from './mail.interface';

@Injectable()
export class MailService {
  // Nodemailer instance
  private readonly mailer: Email;

  constructor(private readonly configService: ConfigService) {
    this.mailer = new Email({
      views: {
        root: __dirname + '/templates',
      },
      transport: {
        service: configService.get('MAIL_TRANSPORT_SERVICE'),
        auth: {
          user: configService.get('MAIL_USERNAME'),
          pass: configService.get('MAIL_PASSWORD'),
        },
      },
      message: {
        from: `${configService.get('MAIL_DEFAULT_FROM_NAME')} <${configService.get('MAIL_DEFAULT_FROM_EMAIL')}>`,
      },
      preview: false,
      juiceResources: {
        preserveImportant: true,
        webResources: {
          relativeTo: path.join(__dirname, 'assets'),
        },
      },
      send: true,
    });
  }

  // Send an email
  public sendMail = async (sendMailOptions: ISendMailOptions): Promise<SentMessageInfo> => {
    try {
      return await this.mailer.send(sendMailOptions);
    } catch (err) {
      // console.log('err', err);
      throw new Error(err);
    }
  };
}
