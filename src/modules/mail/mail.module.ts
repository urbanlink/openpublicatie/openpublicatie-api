import { DynamicModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MailService } from './mail.service';

export const MAILER = 'mailer';
export const MAILER_OPTIONS = 'mailer options';

@Module({})
export class MailModule {
  static register(): DynamicModule {
    return {
      module: MailModule,
      imports: [ConfigModule],
      providers: [MailService],
      exports: [MailService],
    };
  }
}
