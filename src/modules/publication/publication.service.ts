import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindManyOptions, Repository } from 'typeorm';
import { Publication } from './publication.entity';
import { CreatePublicationDto } from './dto/createPublication.dto';
import { UpdatePublicationDto } from './dto/updatePublication.dto';

@Injectable()
export class PublicationService {
  private readonly logger = new Logger(PublicationService.name);

  constructor(
    @InjectRepository(Publication)
    private publicationRepository: Repository<Publication>,
  ) {}


  async findAll(options?: FindManyOptions): Promise<Publication[]> {
    const { take, skip } = options; 
    const publications = await this.publicationRepository.find({
      take, skip,
      relations: ['request']
    });
    if (publications) return publications;

    throw new HttpException('No publications found', HttpStatus.NOT_FOUND);
  }

  async find(id: number): Promise<Publication> {
    const publication = await this.publicationRepository.findOne({ where: { id }, relations: ['request'] });
    if (publication) return publication;

    throw new HttpException('No publication found', HttpStatus.NOT_FOUND);
  }

  async create(data: CreatePublicationDto): Promise<Publication> {
    const newPublication = this.publicationRepository.create(data);
    return await this.publicationRepository.save(newPublication);
  }

  async update(data: UpdatePublicationDto): Promise<Publication> {
    const { id } = data;
    const record = await this.publicationRepository.findOne({ where: { id } });
    if (!record) throw new HttpException('No publications found', HttpStatus.NOT_FOUND);

    return await this.publicationRepository.save({
      ...record,
      ...data,
    });
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.publicationRepository.delete(id);
  }
}
