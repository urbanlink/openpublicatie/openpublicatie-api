import { Body, Controller, Delete, Get, Logger, Param, Put, UseGuards, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from '../auth/auth.guards';
import { CreatePublicationDto } from './dto/createPublication.dto';
import { UpdatePublicationDto } from './dto/updatePublication.dto';
import { PublicationService } from './publication.service';
import { Permissions } from '../auth/permissions.decorator';
import { Publication as PublicationModel} from './publication.entity';

@Controller('publication')
export class PublicationController {
  private readonly logger = new Logger(PublicationController.name);

  constructor(private readonly publicationService: PublicationService) {}

  @Get()
  async findAll(): Promise<PublicationModel[]> {
    return this.publicationService.findAll();
  }

  @Get()
  async find(@Param('id') id: number): Promise<PublicationModel> {
    return this.publicationService.find(id);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Post()
  @Permissions('create:publications')
  async create(@Body('publication') data: CreatePublicationDto) {
    return this.publicationService.create(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Put(':id')
  @Permissions('update:publications')
  async update(@Param() id: number, @Body() data: UpdatePublicationDto) {
    return this.publicationService.update(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:publications')
  async delete(@Param('id') id: number) {
    return this.publicationService.delete(id);
  }
}
