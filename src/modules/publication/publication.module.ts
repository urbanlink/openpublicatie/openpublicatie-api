import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { PublicationController } from './publication.controller';
import { Publication } from './publication.entity';
import { PublicationService } from './publication.service';

@Module({
  imports: [TypeOrmModule.forFeature([Publication]), AuthModule],
  controllers: [PublicationController],
  providers: [PublicationService],
  exports: [PublicationService],
})
export class PublicationModule {}
