import { IsDate, IsOptional, IsString, MaxLength } from 'class-validator';

export class CreatePublicationDto {

  @IsString()
  title: string;
  
  @IsString()
  @IsOptional()
  slug: string;

  @IsString()
  @MaxLength(500)
  description: string;

  @IsDate()
  @IsOptional()
  date_filed: Date

  @IsDate()
  @IsOptional()
  response_deadline: Date

  @IsDate()
  @IsOptional()
  date_finished: Date
}
