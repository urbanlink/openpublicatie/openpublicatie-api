import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn 
} from 'typeorm';
import Request from '../request/request.entity';

@Entity()
export class Publication {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  title: string;

  @Column({
    type: 'text',
    nullable: true
  })
  description: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  created_at: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  updated_at: Date

  @ManyToOne(() => Request, request => request.publications)
  @JoinColumn({name: 'request_id'})
  request: Request;

}
