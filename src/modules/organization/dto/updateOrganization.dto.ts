import { IsBoolean, IsEnum, IsNumber, IsOptional, IsString, MaxLength } from 'class-validator';
import { OrganizationLevel } from '../enum/organizationLevel.enum';

export class UpdateOrganizationDto {
  @IsNumber()
  id: number;

  @IsString()
  title: string;

  @IsString()
  @IsString()
  @MaxLength(500)
  description: string;

  @IsString()
  @IsOptional() 
  @MaxLength(250)    
  slug: string;

  @IsBoolean()
  @IsOptional()
  verified: boolean; 

  @IsString()
  @IsOptional()
  logo: string;

  @IsOptional()
  @IsEnum(OrganizationLevel)
  level: string

  @IsString()
  @IsOptional()
  url: string;

  @IsString()
  @IsOptional()
  twitter: string;

  @IsString()
  @IsOptional()
  portal: string;

  @IsString()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  address: string;
}
