export enum OrganizationLevel {
  MUNICIPALITY = 'Gemeente',
  PROVINCE = 'Provincie',
  WATER_AUTHORIRY = 'Waterschap',
  MINISTRY = 'Ministerie',
  CORPORATE = 'Bedrijf',
  FOUNDATION = 'Stichting' 
}