import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateOrganizationDto } from './dto/createOrganization.dto';
import { UpdateOrganizationDto } from './dto/updateOrganization.dto';
import { Organization } from './organization.entity';

@Injectable()
export class OrganizationService {
  private readonly logger = new Logger(OrganizationService.name);

  constructor(
    @InjectRepository(Organization)
    private organizationRepository: Repository<Organization>,
  ) {}

  async findAll(): Promise<Organization[]> {
    const organizations = await this.organizationRepository.find();
    if (organizations) return organizations;

    throw new HttpException('No organizations found', HttpStatus.NOT_FOUND);
  }

  async find(id: number): Promise<Organization> {
    const organization = await this.organizationRepository.findOne({ where: { id } });
    if (organization) return organization;

    throw new HttpException('No organization found', HttpStatus.NOT_FOUND);
  }

  async create(data: CreateOrganizationDto): Promise<Organization> {
    const newOrganization = this.organizationRepository.create(data);
    return await this.organizationRepository.save(newOrganization);
  }

  async update(data: UpdateOrganizationDto): Promise<Organization> {
    const { id } = data;
    const record = await this.organizationRepository.findOne({ where: { id } });
    if (!record) throw new HttpException('No organizations found', HttpStatus.NOT_FOUND);

    return await this.organizationRepository.save({
      ...record,
      ...data,
    });
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.organizationRepository.delete(id);
  }
}
