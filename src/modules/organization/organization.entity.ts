// Jurisdiction


import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn 
} from 'typeorm';
import Request from '../request/request.entity';
import { OrganizationLevel } from './enum/organizationLevel.enum';

@Entity()
export class Organization {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  title: string;

  @Column({nullable: true, unique: true})
  slug: string;

  @Column({default: false})
  verified: boolean; 

  @Column({nullable: true})
  logo: string;

  @Column({
    type: 'enum',
    enum: OrganizationLevel,
    default: OrganizationLevel.MUNICIPALITY
  })
  level: string

  @Column({nullable: true})
  url: string;

  @Column({nullable: true})
  twitter: string;

  @Column({nullable: true})
  portal: string;

  @Column({nullable: true})
  email: string;

  @Column({nullable: true})
  address: string;

  @Column({
    type: 'text',
    nullable: true
  })
  description: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  created_at: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  updated_at: Date

  @OneToMany(() => Request, request => request.organization)
    @JoinColumn({name: 'request_id'})
  requests: Request[];

}

// Computed values 
// 
// "average_response_time": 159,
// "success_rate": 26.0,
// "number_requests": 46,
// "number_requests_completed": 12,
// "number_requests_rejected": 1,
// "number_requests_no_docs": 22,
// "number_requests_ack": 0,
// "number_requests_resp": 2,
// "number_requests_fix": 4,
// "number_requests_appeal": 0,
// "number_requests_pay": 4,
// "number_requests_partial": 0,
// "number_requests_lawsuit": 0,
// "number_requests_withdrawn": 1