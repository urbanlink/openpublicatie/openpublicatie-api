import { Body, Controller, Delete, Get, Logger, Param, Put, UseGuards, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from '../auth/auth.guards';
import { CreateOrganizationDto } from './dto/createOrganization.dto';
import { UpdateOrganizationDto } from './dto/updateOrganization.dto';
import { OrganizationService } from './organization.service';
import { Permissions } from '../auth/permissions.decorator';
import { Organization as OrganizationModel} from './organization.entity';

@Controller('organization')
export class OrganizationController {
  private readonly logger = new Logger(OrganizationController.name);

  constructor(private readonly organizationService: OrganizationService) {}

  @Get()
  async findAll(): Promise<OrganizationModel[]> {
    return this.organizationService.findAll();
  }

  @Get()
  async find(@Param('id') id: number): Promise<OrganizationModel> {
    return this.organizationService.find(id);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Post()
  @Permissions('create:organizations')
  async create(@Body('organization') data: CreateOrganizationDto) {
    return this.organizationService.create(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Put(':id')
  @Permissions('update:organizations')
  async update(@Param() id: number, @Body() data: UpdateOrganizationDto) {
    return this.organizationService.update(data);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:organizations')
  async delete(@Param('id') id: number) {
    return this.organizationService.delete(id);
  }
}
