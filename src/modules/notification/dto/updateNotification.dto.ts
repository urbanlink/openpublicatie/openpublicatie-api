import { IsString } from 'class-validator';

export class UpdateNotificationDto {

  @IsString()
  public message: string;

}
