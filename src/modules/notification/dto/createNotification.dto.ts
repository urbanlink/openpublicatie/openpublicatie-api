import { IsString, MaxLength } from 'class-validator';

export class CreateNotificationDto {

  @IsString()
  @MaxLength(500)
  message: string;

}
