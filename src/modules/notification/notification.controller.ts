import { Controller, Delete, Get, Logger, Param, UseGuards,} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from '../auth/auth.guards';
import { Permissions } from '../auth/permissions.decorator';
import { NotificationService } from './notification.service';
import { Notification } from './notification.entity';

@Controller('notification')
export class NotificationController {
  private readonly logger = new Logger(NotificationController.name);

  constructor(private readonly notificationService: NotificationService) {}

  @Get()
  async findAll(@Param('take') take: number, @Param('skip') skip: number): Promise<Notification[]> {
    skip = skip || 0; 
    take = take || 100;

    return this.notificationService.findAll({skip, take});
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:notifications')
  async delete(@Param('id') id: number) {
    return this.notificationService.delete(id);
  }
}
