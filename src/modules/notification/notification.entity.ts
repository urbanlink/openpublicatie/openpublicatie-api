import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn 
} from 'typeorm';
import { Request } from '../request/request.entity';

@Entity()
export class Notification {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column({
    type: 'text',
    nullable: true
  })
  message: string;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  created_at: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  updated_at: Date

  @ManyToOne(() => Request, request => request.notifications)
  @JoinColumn({name: 'request_id'})
  request: Request;

}
