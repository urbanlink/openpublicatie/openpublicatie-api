import { HttpException, HttpStatus, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, FindManyOptions, Repository } from "typeorm";
import { Notification } from "./notification.entity";

@Injectable()
export class NotificationService {
  private readonly logger = new Logger(NotificationService.name);

  constructor(
    @InjectRepository(Notification)
    private notificationRepository: Repository<Notification>
  ) {}

  async findAll(options?: FindManyOptions): Promise<Notification[]> {
    const { take, skip } = options; 

    const notifications = await this.notificationRepository.find({
      take, skip,
      relations: ['request']
    });
    if (notifications) return notifications;

    throw new HttpException('No notifications found', HttpStatus.NOT_FOUND);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.notificationRepository.delete(id);
  }
}
