import { HttpException, HttpStatus, Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { DeleteResult, Repository } from "typeorm";
import { Communication } from "./communication.entity";

@Injectable()
export class CommunicationService {
  private readonly logger = new Logger(CommunicationService.name);

  constructor(
    @InjectRepository(Communication)
    private communicationRepository: Repository<Communication>
  ) {}

  async findAll(): Promise<Communication[]> {
    const communications = await this.communicationRepository.find();
    if (communications) return communications;

    throw new HttpException('No communications found', HttpStatus.NOT_FOUND);
  }

  async delete(id: number): Promise<DeleteResult> {
    return await this.communicationRepository.delete(id);
  }
}
