import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn 
} from 'typeorm';
import { Request } from '../request/request.entity';

@Entity()
export class Communication {

  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  subject: string;

  @Column({
    type: 'text',
    nullable: true
  })
  body: string;

  @Column({
    type: 'timestamptz',
    nullable: true
  })
  send_date: Date

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz'
  })
  created_at: Date

  @UpdateDateColumn({
    name: 'updated_at',
    type: 'timestamptz'
  })
  updated_at: Date

  @ManyToOne(() => Request, request => request.communications)
  @JoinColumn({ name: 'request_id' })
  request: Request;

}
