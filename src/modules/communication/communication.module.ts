import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../auth/auth.module';
import { CommunicationController } from './communication.controller';
import { Communication } from './communication.entity';
import { CommunicationService } from './communication.service';

@Module({
  imports: [TypeOrmModule.forFeature([Communication]), AuthModule],
  controllers: [CommunicationController],
  providers: [CommunicationService],
  exports: [CommunicationService]
})
export class CommunicationModule {}
