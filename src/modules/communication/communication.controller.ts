import {
  // Body,
  Controller, Delete, Get, Logger, Param, UseGuards,
  // Post, Req, HttpException, HttpStatus
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsGuard } from '../auth/auth.guards';
import { Permissions } from '../auth/permissions.decorator';
import { AuthService } from '../auth/auth.service';
import { CommunicationService } from './communication.service';
import { Communication } from './communication.entity';

@Controller('communication')
export class CommunicationController {
  private readonly logger = new Logger(CommunicationController.name);

  constructor(private readonly communicationService: CommunicationService, private readonly authService: AuthService) {}

  @Get()
  async findAll(): Promise<Communication[]> {
    return this.communicationService.findAll();
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:communications')
  async delete(@Param('id') id: number) {
    return this.communicationService.delete(id);
  }
}
