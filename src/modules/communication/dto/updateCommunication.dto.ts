import { IsNumber, IsString, Length } from 'class-validator';

export class UpdateCommunicationDto {
  @IsNumber()
  public id: number;

  @IsString()
  @Length(3, 150)
  public title: string;

  @IsString()
  public description: string;

}
