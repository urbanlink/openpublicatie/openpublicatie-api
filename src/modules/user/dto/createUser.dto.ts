import { IsBoolean, IsDate, IsEmail, IsOptional, IsString, Length, MaxLength } from 'class-validator';
import { Escape, Trim } from 'class-sanitizer';

export class CreateUserDto {
  @IsString()
  public auth0: string;

  @IsEmail()
  public email: string;

  @IsString()
  @Length(3, 25)
  @Escape()
  @Trim()
  public nickname: string;

  @IsOptional()
  @IsString()
  @Length(3, 25)
  slug?: string;

  @IsOptional()
  @IsString()
  picture?: string;

  @IsOptional()
  @IsString()
  account_plan?: string;

  @IsOptional()
  @IsDate()
  @IsOptional()
  account_plan_end?: Date;

  @IsOptional()
  @IsBoolean()
  admin?: boolean;

  @IsOptional()
  @IsString()
  @MaxLength(15)
  @Escape()
  @Trim()
  firstname?: string;

  @IsOptional()
  @IsString()
  @MaxLength(20)
  @Escape()
  @Trim()
  lastname?: string;

  @IsOptional()
  @IsString()
  @MaxLength(300)
  @Escape()
  @Trim()
  bio?: string;

  @IsOptional()
  @IsDate()
  signed_up?: Date;
}
