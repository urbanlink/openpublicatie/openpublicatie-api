import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToMany } from 'typeorm';
import Request from '../request/request.entity';

const DAILY = (60*24);
// const WEEKLY = (60*24*7);
// const MONTHLY = (60*24*30);

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  readonly id!: number;

  @Column({ unique: true })
  auth0?: string;

  @Column({
    unique: true,
    select: false,
  })
  email: string;

  @Column({ unique: true })
  nickname: string;

  @Column({ unique: true, nullable: true })
  slug: string;

  @Column({nullable: true})
  picture: string;

  @Column({nullable:true})
  account_plan: string;

  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  account_plan_end: Date;

  @Column({ default: false })
  admin!: boolean;

  @Column({ type: 'text', nullable: true })
  bio: string;

  @Column({ nullable: true })
  firstname: string;

  @Column({ nullable: true })
  lastname: string;

  @Column({ nullable: true, default: DAILY })
  notification_schedule: number;

  @Column({ nullable: true })
  last_email_notification: Date;

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz',
  })
  createdAt: Date;

  @OneToMany(() => Request, request => request.owner)
  requests: Request[];
}
