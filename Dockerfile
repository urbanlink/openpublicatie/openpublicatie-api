FROM node:lts-alpine as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

# Copy build files from previous job 
COPY dist package.json yarn.lock ./

# Install production modules
RUN yarn install --prod 

CMD ["node", "main"]
